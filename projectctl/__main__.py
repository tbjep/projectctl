import argparse

def get_roots():
    return [
            "~/Projekter/"
    ]

def addroot_command(args):
    print('Adding roots: ', args.root)

def get_command(args):
    print('Getting projects...')

def main():
    parser = argparse.ArgumentParser(description='Manage project locations.')
    parser.set_defaults(func=(lambda args: parser.print_help()))
    subp = parser.add_subparsers()
    addroot_parser = subp.add_parser('addroots', aliases=['ar', 'addroot'])
    addroot_parser.add_argument('root', nargs='+')
    addroot_parser.set_defaults(func=addroot_command)
    get_parser = subp.add_parser('get')
    get_parser.add_argument('path', nargs='?')
    get_parser.set_defaults(func=get_command)
    args = parser.parse_args()
    args.func(args)

if __name__ == '__main__':
    main()
